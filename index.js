const express = require("express");
const { inherits } = require("util");
const app = express();
const port = 7000;
app.get("/add/:numbers", (req, res) => {
    const numbers = req.params.numbers.split(",");
    const sum = numbers.reduce((accumulator, currentSum) => {
      return accumulator + parseInt(currentSum);
    }, 0);
    res.send(`The sum of the numbers is ${sum}`);
  });
  app.listen(port, () => {
    console.log(`Sum API listening at http://localhost:${port}`);
  });
  app.get("/multiply/:numbers", (req, res) => {
    const numbers = req.params.numbers.split(",");
    const product = numbers.reduce((accumulator, currentProduct) => {
      return accumulator * parseInt(currentProduct);
    }, 1);
    res.send(`The product of the numbers is ${product}`);
  });
  app.listen(port, () => {
    console.log(`Product API listening at http://localhost:${port}`);
  });
app.listen(port, () => {
  console.log(`Calculator app listening at http://localhost:${port}`);
});